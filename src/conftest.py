# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import asyncio

import dramatiq  # noqa
import pytest
from dramatiq import Worker

from conf.dramatiq_conf import broker
from main import build_application

# register fixtures from my modules
from tests.core_tests.fixtures import *  # noqa


# [NGibaev 11.04.19] loop should be instantiated only once per test session
@pytest.fixture(scope="session")
def loop():
    """
    Fixture for general asyncio loop
    """
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def app(loop):
    """
    Fixture for service Application instance, but server still not started
    """
    _app = loop.run_until_complete(build_application())
    yield _app


@pytest.fixture
def app_server(loop, app, aiohttp_server):
    """
    Fixture for running whole application (ready for requests)
    """
    return loop.run_until_complete(aiohttp_server(app))


@pytest.fixture
def app_client(loop, app, aiohttp_client):
    """
    Fixture for testing server instance with requests
    """
    return loop.run_until_complete(aiohttp_client(app))


def register(app, routes):
    for method, path, handler, name in routes:
        app.router.add_route(method, path, handler, name=name)


@pytest.fixture
def stub_broker():
    broker.flush_all()
    return broker


@pytest.fixture
def stub_worker():
    worker = Worker(broker, worker_timeout=100)
    worker.start()
    yield worker
    worker.stop()
