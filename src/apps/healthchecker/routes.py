# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from . import views

routes = (
    ('GET', '/healthcheck', views.healthcheck, 'healthcheck'),
    ('GET', '/', views.index, 'index'),
)
