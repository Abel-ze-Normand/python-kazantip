# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import ujson

from typing import Tuple
from functools import partial

from asyncpg.pool import Pool

from common.newrelic_shortcuts import nr_timeit, nr_save_attr
from common.utils.exceptions import PaginationOverflow
from common.utils.utils import change_dict_naming_convention


class AbstractDAO:
    @property
    def has_collect(self):
        return bool(self.collect_spec)

    @property
    def has_save(self):
        return bool(self.save_spec)

    @property
    def has_update(self):
        return bool(self.update_spec)

    @property
    def has_delete(self):
        return bool(self.delete_spec)


class BaseSnakeCaseCursorCopyDAO(AbstractDAO):
    """
    Base DAO class for implementing streaming data via cursor into external sources.
    Its required that ``collect_spec`` returns set of json/jsonb objects.
    Each object in resulting stream will have ``snake_case`` formatted keys
    """
    collect_spec = None

    def __init__(self, pool: Pool, response, **_):
        self._pool = pool
        self.response = response

    async def _copy_from_query(self, q, args, callback: callable, **_):
        """
        Iterate over each query result and execute callback over each row
        """
        async with self._pool.acquire() as con:
            async with con.transaction():
                async for r in con.cursor(q, *args):
                    await callback(r)

    async def _write_to_stream_response(self, response, row: dict):
        row_raw = bytearray(row['row'], encoding='utf8') + b'\n'
        await response.write(row_raw)

    async def collect(self, data):
        q, args = self.collect_spec(**data).collect_q_str
        await self._copy_from_query(q, args,
                                    partial(self._write_to_stream_response, self.response))


class BaseCamelCaseCursorCopyDAO(AbstractDAO):
    """
    Base DAO class for implementing streaming data via cursor into external sources.
    Its required that ``collect_spec`` returns set of json/jsonb objects.
    Each object in resulting stream will have ``camelCase`` formatted keys
    """
    collect_spec = None

    def __init__(self, pool: Pool, response, **_):
        self._pool = pool
        self.response = response

    async def _copy_from_query(self, q, args, callback: callable, **_):
        """
        Iterate over each query result and execute callback over each row
        """
        async with self._pool.acquire() as con:
            async with con.transaction():
                async for r in con.cursor(q, *args):
                    await callback(r)

    async def _write_to_stream_response(self, response, row: dict):
        camelcased_row = ujson.dumps(change_dict_naming_convention(row['row']))
        row_raw = bytearray(camelcased_row, encoding='utf8') + b'\n'
        await response.write(row_raw)

    async def collect(self, data):
        q, args = self.collect_spec(**data).collect_q_str
        await self._copy_from_query(q, args,
                                    partial(self._write_to_stream_response, self.response))


class AdapterDbQuery:
    def __init__(self, pool: Pool, **_):
        self._pool = pool

    async def fetch(self, query: str, *args, timeout: int=None, as_dicts: bool=False) -> list:
        """
        Execute query and return resulting set
        :param str query: SQL query string
        :param args: query parameters
        :param float timeout: query execution timeout. Default - infinity
        :param bool as_dicts: format each row in ``dict``
        :return: list of ``Record`` objects or dicts (if ``as_dicts`` set)
        """
        nr_save_attr('DB.query', query)
        nr_save_attr('DB.params', args)
        records = await self._pool.fetch(query, *args, timeout=timeout)
        if records and as_dicts:
            return [{k: v for k, v in record.items()} for record in records]
        return records

    async def fetchval(self, query: str, *args, column: int=0, timeout: int=None) -> object:
        """
        Execute query and return value from first row and first attribute of resulting set
        :param str query: SQL query string
        :param args: query parameters
        :param float timeout: query execution timeout. Default - infinity
        :param bool as_dicts: format each row in ``dict``
        :return: value from first row and first attribute
        """
        nr_save_attr('DB.query', query)
        nr_save_attr('DB.params', args)
        return await self._pool.fetchval(query, *args, column=column,
                                         timeout=timeout)

    async def fetchrow(self, query: str, *args, timeout: int=None, as_dicts: bool=False):
        """
        Execute query and return first row
        :param str query: SQL query string
        :param args: query parameters
        :param float timeout: query execution timeout. Default - infinity
        :param bool as_dicts: format each row in ``dict``
        :return: ``Record`` instance or dict (if ``as_dicts`` set)
        """
        nr_save_attr('DB.query', query)
        nr_save_attr('DB.params', args)
        record = await self._pool.fetchrow(query, *args, timeout=timeout)
        if record and as_dicts:
            return {k: v for k, v in record.items()}
        return record


class BaseDAO(AbstractDAO):
    """
    General aggregation class for multiple ``Spec`` and ``ModifyingSpec`` class references.
    Unions 4 of them for each CRUD operation respectivly.

    Basic ``BaseDAO`` implementation contains ``_fields`` field set to dict of all fields with serialization functions,
    and set ``{collect,save,update,delete}_spec`` field for working same named method.
    """
    _fields: dict = {}
    _exclude_fields: tuple = ()
    collect_spec = None
    save_spec = None
    update_spec = None
    delete_spec = None

    def __init__(self, pool: Pool, **_):
        self._pool = pool

    @classmethod
    def available_fields(cls) -> Tuple[str]:
        """
        Returns all registered fields in DAO
        :return: tuple of field names
        """
        return tuple(cls._fields.keys())

    def _serialize_field(self, field_name: str, value: str) -> object:
        v = self._fields[field_name](value)
        return v

    def _serialize_items(self, items: list) -> list:
        item_list = [
            {
                field: self._serialize_field(field, value)
                for field, value in item.items()
                if field in self._fields
            }
            for item in items
        ]
        return item_list

    @nr_timeit("DB_collect")
    async def collect(self, **spec_args) -> dict:
        """
        Method for data retrieval from DB.

        When ``BaseDAO`` class bound to ``View`` by its declaration, this method is executed on incoming ``GET`` requests.
        Calls ``collect_spec``.

        N.B. this method executes 2 queries per call -- for items and for total items count (for proper pagination)

        :param data: object passed as kwargs to ``collect_spec`` constructor
        :return: dict with ``total`` and ``items`` keys
        """
        spec = self.collect_spec(**spec_args)
        sql, params = spec.collect_q_str
        items = await AdapterDbQuery(self._pool).fetch(sql, *params)
        sql, params = spec.total_q_str
        total = await AdapterDbQuery(self._pool).fetchval(sql, *params)
        offset = spec_args.get("offset")
        if total >= 0 and offset and offset >= total:
            raise PaginationOverflow(offset, total)
        return {
            'total': total,
            'items': self._serialize_items(items),
        }

    @nr_timeit("DB_save")
    async def save(self, data: dict, *spec_args, **spec_kwargs) -> dict:
        """
        Method for inserting record(s) in DB. Actual saving behaviour is defined by ``save_spec``.
        If result of query execution generated by ``save_spec`` contains rows then returns them and formats them
        alike with ``collect`` result formatting behaviour

        :param data: object that will be passed to ``save`` method of ``save_spec``
        :param args: arguments that will be passed to ``save_spec`` constructor
        :param kwargs: keyword arguments that will be passed to ``save_spec`` constructor
        :return: list ``Record`` instances
        """
        spec = self.save_spec(data, *spec_args, **spec_kwargs)
        sql, params = spec.save_q_str
        save = await AdapterDbQuery(self._pool).fetch(sql, *params, as_dicts=True)
        return save

    @nr_timeit("DB_update")
    async def update(self, data: dict, *spec_args, **spec_kwargs) -> dict:
        spec = self.update_spec(data, *spec_args, **spec_kwargs)
        sql, params = spec.update_q_str
        update = await AdapterDbQuery(self._pool).fetch(sql, *params, as_dicts=True)
        return update

    @nr_timeit("DB_delete")
    async def delete(self, *spec_args, **spec_kwargs) -> dict:
        spec = self.delete_spec(*spec_args, **spec_kwargs)
        sql, params = spec.delete_q_str
        delete = await AdapterDbQuery(self._pool).fetch(sql, *params, as_dicts=True)
        return delete
