# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging

import newrelic.agent
from datetime import datetime
from functools import wraps

from conf.settings import SERVICE_NAME

logger = logging.getLogger(__name__)


def metric(name, value):
    yield f'Custom/{SERVICE_NAME}_{name}/', value


def nr_agent_metric(name: str, value: object):
    try:
        newrelic.agent.record_custom_metrics(metric(name, value))
        logger.info("metrics collected")
    except Exception as e:
        logger.warning(f"could not collect metrics: {e}")


def nr_timeit_fun(name: str, fun: callable, *args, **kwargs):
    ts = datetime.now()
    result = fun(*args, **kwargs)
    te = datetime.now()
    value = (te.microsecond - ts.microsecond)/1000
    nr_agent_metric(name, value)
    nr_save_attr(f'fun.call.{name}', value)
    return result


def nr_timeit(name: str):
    def method_wrapper(method):
        @wraps(method)
        def wrap(*args, **kw):
            ts = datetime.now()
            result = method(*args, **kw)
            te = datetime.now()
            value = (te.microsecond - ts.microsecond)/1000
            nr_agent_metric(name, value)
            return result
        return wrap
    return method_wrapper


def nr_save_attr(attr: str, attr_v: object):
    newrelic.agent.add_custom_parameter(attr, attr_v)
