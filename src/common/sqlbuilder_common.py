# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from functools import partial

from sqlbuilder.smartsql import (Cast, Constant, F, Q, T, TableJoin, Value,
                                 compile, func, Array, Param, Binary)

from .custom_spec_compiler import SQLParam


def extr_path(target, *args, as_type=None, extractor=func.jsonb_extract_path):
    e = extractor(target, *[V(arg) for arg in args])
    return Cast(e, as_type) if as_type else e

dig, dig_t = extr_path, partial(extr_path, extractor=func.jsonb_extract_path_text)
V, Array, Param, Constant, F, Q, TableJoin, compile, SQLParam = Value, Array, Param, Constant, F, Q, TableJoin, compile, SQLParam

DOUBLE, BOOLEAN, INTEGER, TEXT = 'double precision', 'boolean', 'bigint', 'text'

# [NGibaev 30.08.18] use only for upserts
EXCLUDED_T = T.excluded
