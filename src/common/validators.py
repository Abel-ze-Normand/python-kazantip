# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging
from typing import Dict, Tuple

import arrow
import dateutil
import inflection
from schema import And, Const, Optional, Or, Regex, Schema, Use

from common.utils.exceptions import HTTPBadRequest
from common.utils.utils import (camel_to_underscore,
                                change_dict_naming_convention)
from conf.settings import MIN_LIMIT, MIN_OFFSET

logger = logging.getLogger(__name__)


def string_validator(field_name: str, max_length=None) -> Use:
    return And(Use(str, error=f"{field_name} must be string."),
               lambda x: (not max_length) or (len(x) <= max_length))


def search_phrase_validator(field_name: str) -> And:
    return And(Const(Regex(r'^[- \w]+$')),
               Use(lambda x: x.strip()),
               error=f"'{field_name}' must be set of letters, numbers, space, "
                     f"hyphen and underscore")


def string_array_validator(field_name: str) -> And:
    def is_string_item(iterable):
        for item in iterable:
            if not isinstance(item, str):
                return False
        return True

    return And(Use(list, error=f"{field_name} must be array of strings."), is_string_item)


def force_integer_validator(field_name: str) -> And:
    return And(Use(int), And(lambda n: n >= 0), error=f"'{field_name}' must be integer >= 0")


def bin_validator(field_name: str) -> And:
    return \
        And(
            And(Use(int), And(lambda n: n >= 0 and n <= 1), error=f"'{field_name}' must be 1 or 0"),
            Use(bool)
        )


def csv_int_values_validator(field_name: str) -> And:
    return And(Regex(r'^\d+?(,\d+)*$'),
               Use(lambda x: tuple(map(int, x.split(',')))),
               error=f"'{field_name}' must be integer values separated by a comma")


def csv_values_validator(field_name: str, values: Tuple[str]) -> And:
    available_values = set(values)
    printed_avail_values = {inflection.camelize(f, False) for f in available_values}
    return And(Use(str),
               Use(lambda x: set(x.split(','))),
               Use(lambda x: {inflection.underscore(f) for f in x}),
               Const(lambda x: x.issubset(available_values)),
               Use(lambda x: list(x)),
               error=f"'{field_name}' must be any values of {printed_avail_values} separated by comma")


def csv_words_values_validator(field_name: str) -> And:
    return And(Regex(r'^[\w_]+?(,[\w_]+)*$'),
               Use(lambda x: tuple(map(str, x.split(',')))),
               error=f"'{field_name}' must be words with underscores separated by a comma")


def items_per_page_validator(field_name: str) -> Or:
    return Or(force_integer_validator(field_name), lambda _: MIN_LIMIT)


def start_index_validator(field_name: str) -> Or:
    return Or(force_integer_validator(field_name), lambda _: MIN_OFFSET)


def object_validator(field_name: str) -> Use:
    return Use(dict, error=f"'{field_name}' must be an object.")


def array_validator(field_name: str) -> Use:
    return Use(list, error=f"'{field_name}' must be array of objects.")


def bool_querystring_validator(field_name: str) -> Use:
    return Use(lambda x: x in ['true', 'True'], error=f"'{field_name} must be True or False.")


def bool_body_validator(field_name: str) -> Use:
    return Use(bool, error=f"'{field_name} must be true or false.")


def svyabk_validator(field_name: str) -> Regex:
    SVYABK_RE_STR = r'^(([A-Z]+|\*)-){2}([a-z]+|\*)-([A-Z]+|\*)-[^\-]*$'
    return Regex(SVYABK_RE_STR,
                 error=f"'{field_name}' must be valid svyabk.")


def datetime_to_utc(date):
    """Returns date in UTC w/o tzinfo"""
    return date.astimezone(dateutil.tz.gettz('UTC')).replace(tzinfo=None) if date.tzinfo else date


def __validate_data(date: str) -> object:
    try:
        date_time = dateutil.parser.parse(date)
        date_time_utc = datetime_to_utc(date_time)
        return date_time_utc
    except Exception as e:
        return None


def datetime_validator(field_name: str) -> Use:
    return Use(lambda date: __validate_data(date),
               error=f"'{field_name}' must be datetime.")


class BaseValidator(object):
    _base_schema = {
        Optional('itemsPerPage'): items_per_page_validator('itemsPerPage'),
        Optional('startIndex'): start_index_validator('startIndex'),
        Optional('ids'): csv_int_values_validator('ids')
    }
    _base_rename_keys_map = {
        'itemsPerPage': 'limit',
        'startIndex': 'offset',
        'businessId': 'business_id',
        'countryId': 'country_id',
    }
    _extra_schema = {}
    _extra_rename_keys_map = {}
    _schema: Schema
    _rename_keys_map: Dict[str, str]

    _extra_body_schema = {}

    def __init__(self, ignore_extra_keys=False):
        # issue with optional keys merge
        # {**{'q': int}, **{Optional('q'): int}} -> {'q': int, Optional('q'): int}
        self._schema = Schema({**self._base_schema, **self._extra_schema},
                              ignore_extra_keys=ignore_extra_keys)
        self._rename_keys_map = {**self._base_rename_keys_map,
                                 **self._extra_rename_keys_map}
        self._body_schema = Schema(self._extra_body_schema)

    def _rename_keys(self, old_dict: dict) -> dict:
        new_dict = old_dict.copy()
        for old_key, new_key in self._rename_keys_map.items():
            if old_key in new_dict:
                new_dict[new_key] = new_dict.pop(old_key)
        return new_dict

    def _has_rename_key_map(self):
        return isinstance(self._rename_keys_map, dict) and self._rename_keys_map

    def validate(self, data):
        try:
            validated_data = self._schema.validate(data)
        except Exception as e:
            raise HTTPBadRequest(message=str(e))
        else:
            if isinstance(validated_data, dict) and self._has_rename_key_map():
                return self._rename_keys(validated_data)
            else:
                return validated_data

    def validate_body(self, data):
        try:
            validated_data = self._body_schema.validate(data)
        except Exception as e:
            raise HTTPBadRequest(message=str(e))
        else:
            if isinstance(validated_data, dict) and self._has_rename_key_map():
                validated_data = self._rename_keys(validated_data)
            return change_dict_naming_convention(validated_data, camel_to_underscore)
