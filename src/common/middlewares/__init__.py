# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from .log_middleware import log_middleware


def get_middlewares():
    list_middlewares = (log_middleware,)
    return list_middlewares
