# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from schema import Optional, Or, Schema


def get_api_schema(items_schema: Schema = None) -> Schema:
    data = Schema({
        "kind": str,
        Optional("fields"): str,
        Optional("currentItemCount"): int,
        Optional("itemsPerPage"): int,
        Optional("startIndex"): int,
        Optional("nextLink"): str,
        Optional("totalItems"): int,
        "items": Or([items_schema], list)
    })

    api_schema = Schema({
        "apiVersion": str,
        Optional("context"): str,
        Optional("id"): str,
        Optional("method"): str,
        "data": data
    })

    return api_schema
