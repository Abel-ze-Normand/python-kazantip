# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


# integration tests for models (DAO) and their integration with specs

# tests require test table. in order to run this test suite, create test table:
# CREATE TABLE __users (id bigint not null primary key, name varchar(100) not null, username varchar(40) not null, password varchar(255) not null);

import pytest
from typing import Type
from common.utils.utils import dict_dig
from common.utils.exceptions import PaginationOverflow


@pytest.fixture
def test_db(request, loop, app):
    def cleanup():
        loop.run_until_complete(
            app.pool.execute("TRUNCATE TABLE __users;")
        )

    cleanup()  # fresh start
    loop.run_until_complete(
        app.pool.execute("""
        INSERT INTO __users VALUES
        (1, 'test_name', 'test_username', 'passw0rd'),
        (2, 'test_name_', 'test_username_', 'passw0rd');
        CLUSTER __users USING __users_pkey;
        """)
        # [NGibaev 11.04.19] cluster index to preserve order
    )
    request.addfinalizer(cleanup)


async def test_collect_all(test_db, app, test_users_dao: Type['TestUsersDAO']):
    resp = await test_users_dao(app.pool).collect()
    assert 'items' in resp
    assert 'total' in resp
    assert len(resp['items']) == resp['total']
    assert resp['items']
    for item in resp['items']:
        # [NGibaev 11.04.19] all fields should be returned
        assert list(item.keys()) == list(test_users_dao.available_fields())


async def test_collect_selected_fields(test_db, app, test_users_dao: Type['TestUsersDAO']):
    allowed_fields = ['id', 'name']
    resp = await test_users_dao(app.pool).collect(fields=allowed_fields)
    for item in resp['items']:
        assert all(k in allowed_fields for k in item)


async def test_collect_paginated(test_db, app, test_users_dao: Type['TestUsersDAO']):
    limit = 1
    resp = await test_users_dao(app.pool).collect(limit=limit, offset=0)
    assert len(resp['items']) == limit
    assert dict_dig(resp, ['items', 0, 'id']) == 1

    # [NGibaev 11.04.19] next page
    resp = await test_users_dao(app.pool).collect(limit=limit, offset=1)
    assert len(resp['items']) == limit
    assert dict_dig(resp, ['items', 0, 'id']) == 2

    # [NGibaev 11.04.19] next page, however, does not exist
    with pytest.raises(PaginationOverflow):
        await test_users_dao(app.pool).collect(limit=limit, offset=2)

    resp = await test_users_dao(app.pool).collect(limit=100, offset=0)
    assert len(resp['items']) == resp['total']


async def test_ids_filter(test_db, app, test_users_dao: Type['TestUsersDAO']):
    ids = [1, ]
    resp = await test_users_dao(app.pool).collect(ids=ids)
    assert len(resp['items']) == len(ids)
    assert dict_dig(resp, ['items', 0, 'id']) == ids[0]


async def test_mod_spec_save(test_db, app, test_users_dao: Type['TestUsersDAO']):
    user_data = dict(id=3, name='abc', username='abc', password='passw0rd')
    resp_save = await test_users_dao(app.pool).save(user_data)
    # [NGibaev 11.04.19] bcs returnings are not affected by ``_fields`` field in DAO
    assert len(list(resp_save[0].keys())) == 4
    resp = await test_users_dao(app.pool).collect(ids=[user_data['id'], ])
    assert resp['items']


async def test_mod_spec_update(test_db, app, test_users_dao: Type['TestUsersDAO']):
    user_data = dict(id=1, name='abc', username='abc', password='passw0rd')
    resp_save = await test_users_dao(app.pool).update(user_data, ids=[user_data['id'], ])
    assert len(list(resp_save[0].keys())) == 4
    resp = await test_users_dao(app.pool).collect(ids=[user_data['id'], ])
    assert dict_dig(resp, ['items', 0, 'name']) == user_data['name']
    assert dict_dig(resp, ['items', 0, 'username']) == user_data['username']


async def test_mod_spec_delete(test_db, app, test_users_dao: Type['TestUsersDAO']):
    ids_to_del = [1, ]
    resp_delete = await test_users_dao(app.pool).delete(ids=ids_to_del)
    assert dict_dig(resp_delete, [0, 'id']) == ids_to_del[0]
    resp = await test_users_dao(app.pool).collect()
    assert set(map(lambda x: x['id'], resp['items'])).isdisjoint(set(ids_to_del))
