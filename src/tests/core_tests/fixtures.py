# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import pytest

from collections import OrderedDict
from schema import Optional
from typing import Type

from common.base_dao import BaseDAO
from common.base_view import BaseView
import common.serializators as s
from common.spec import Spec, ModifyingSpec
from common.service_base import Service, ServiceResult, curry
from common.controller import Controller
from common.sqlbuilder_common import T, V, Q, func, INTEGER, EXCLUDED_T, Array, SQLParam, INTEGER
from common.validators import BaseValidator, string_validator, force_integer_validator, csv_values_validator


@pytest.fixture
def test_select_spec() -> Type['TestSelectSpec']:
    class TestSelectSpec(Spec):
        _t = T.table
        _fields = OrderedDict((
            ('fld1', _t.fld1),
            ('fld2', func.ROW_TO_JSON(func.ROW(_t.fld2, _t.fld3))),
        ))

        def __init__(self, *args, my_arg=None, **kwargs):
            if not my_arg:
                raise Exception("NO MY ARG")
            self.my_arg = my_arg
            super().__init__(*args, **kwargs)

        def main_tables(self):
            return self._t

        def ids_filter(self, ids_t):
            return (ids_t.as_('id'), self._t.id, INTEGER)

        def apply_criteria(self, q):
            return q.where(self._t.fld1 == self.my_arg)

    return TestSelectSpec


@pytest.fixture
def test_mod_spec() -> Type['TestModSpec']:
    class TestModSpec(ModifyingSpec):
        _target_tbl = T.table
        _insert_on_conflict = {
            _target_tbl.fld_conflict: EXCLUDED_T.fld_conflict,
        }
        _duplicate_key = (_target_tbl.dup_fld,)
        _returning = (_target_tbl.fld, _target_tbl.dup_fld, _target_tbl.fld_conflict)
        _fields = (_target_tbl.fld, _target_tbl.dup_fld, _target_tbl.fld_conflict)

        def __init__(self, data=None, filter_ids=None, **kwargs):
            self.filter_ids = filter_ids
            super().__init__(data=data, **kwargs)

        def prepare_data(self, data):
            return (data["a"], data["b"], Array(data["c"]))

        def prepare_data_update(self, data):
            return {
                self._target_tbl.fld: data["a"],
                self._target_tbl.dup_fld: data["b"],
                self._target_tbl.fld_conflict: Array(data["c"]),
            }

        def apply_criteria(self, q):
            return q.where(self._target_tbl.fld.in_(self.filter_ids))

    return TestModSpec


@pytest.fixture
def test_mod_subq_spec() -> Type['TestModSubqSpec']:
    class TestModSubqSpec(ModifyingSpec):
        _target_tbl = T.table
        _fields = (_target_tbl.fld, _target_tbl.dup_fld, _target_tbl.fld_conflict)

        def prepare_data(self, data):
            return (data["a"], data["b"], Q().fields(func.array_agg(1)).tables(T.other_table))

        def prepare_data_update(self, data):
            return {
                self._target_tbl.fld: data["a"],
                self._target_tbl.dup_fld: data["b"],
                self._target_tbl.fld_conflict: Q().fields(func.array_agg(1)).tables(T.other_table),
            }

    return TestModSubqSpec


@pytest.fixture
def test_users_spec() -> Type['TestUsersSpec']:
    class TestUsersSpec(Spec):
        _target_tbl = T('__users').as_('u')
        _fields = {
            'id': _target_tbl.id,
            'name': _target_tbl.name,
            'username': _target_tbl.username,
            'password': _target_tbl.password,
        }

        def ids_filter(self, ids_t):
            return (ids_t.as_('uid'), self._target_tbl.id, INTEGER)

        def main_tables(self):
            return self._target_tbl

    return TestUsersSpec


@pytest.fixture
def test_users_mod_spec() -> Type['TestUsersModSpec']:
    class TestUsersModSpec(ModifyingSpec):
        _target_tbl = T('__users').as_('u')
        _fields = (_target_tbl.id, _target_tbl.name, _target_tbl.username, _target_tbl.password)
        _returning = _fields

        def __init__(self, data=None, ids=None, **kwargs):
            self.filter_ids = ids
            # [NGibaev 12.04.19] dont do this in real code
            if 'id' in (data or {}):
                self.filter_ids = [data['id'], ]
            super().__init__(data=data, **kwargs)

        def prepare_data(self, data):
            return (data['id'], data['name'], data['username'], data['password'])

        def prepare_data_update(self, data):
            return {
                'name': data['name'],
                'username': data['username'],
                'password': data['password'],
            }

        def apply_criteria(self, q):
            return q.where(self._target_tbl.id == func.ANY(Array(self.filter_ids)))

    return TestUsersModSpec


@pytest.fixture
def test_users_dao(test_users_spec: Type['TestUsersSpec'], test_users_mod_spec: Type['TestUsersModSpec']) -> Type['TestUsersDAO']:
    class TestUsersDAO(BaseDAO):
        _fields = {'id': s.as_is, 'name': s.as_is, 'username': s.as_is}
        collect_spec = test_users_spec
        save_spec = test_users_mod_spec
        update_spec = test_users_mod_spec
        delete_spec = test_users_mod_spec

    return TestUsersDAO


@pytest.fixture
def test_user_data_validator(test_users_dao: Type['TestUsersDAO']) -> Type['UserDataValidator']:
    class UserDataValidator(BaseValidator):
        _extra_schema = {
            Optional('id'): force_integer_validator('id'),
            Optional('name'): string_validator('name'),
            Optional('username'): string_validator('username'),
            Optional('password'): string_validator('password'),
            Optional('fields'): csv_values_validator('fields', test_users_dao.available_fields()),
        }

        _extra_body_schema = _extra_schema

    return UserDataValidator


@pytest.fixture
def test_users_view(test_users_dao: Type['TestUsersDAO'], test_user_data_validator: Type['UserDataValidator']) -> Type['TestUsersView']:
    class TestUsersView(BaseView):
        # [NGibaev 12.04.19] in real applications I would NOT recommend to use one validator
        # for all methods (its error prone and high-coupled)
        get_validator = test_user_data_validator
        post_validator_body = test_user_data_validator
        put_validator_body = test_user_data_validator
        delete_validator = test_user_data_validator
        model = test_users_dao

    return TestUsersView


@pytest.fixture
def test_get_all_users_svc(test_users_dao):
    class GetAllUsersService(Service):
        def __init__(self, q_params, pool=None):
            self.q_params = q_params
            self.pool = pool

        async def call(self):
            return ServiceResult(result_args=[(await test_users_dao(self.pool).collect())['items']])

    return GetAllUsersService


@pytest.fixture
def new_name():
    return "NEW NAME"


@pytest.fixture
def test_chg_names_users_svc(new_name):
    class ChangeUsersNamesService(Service):
        def __init__(self, users):
            self.users = users

        async def call(self):
            return ServiceResult(result_args=[
                {'items': [{**x, **{'name': new_name}} for x in self.users]}
            ])

    return ChangeUsersNamesService


@pytest.fixture
def test_get_users_w_chg_names_controller(test_get_all_users_svc, test_chg_names_users_svc):
    class TestController(Controller):
        def prepare_input(self, params):
            return params

        def pipeline(self, params, request):
            return [
                curry(test_get_all_users_svc, pool=request.app.pool),
                test_chg_names_users_svc,
            ]

        def unpack_last_result(self, last_result):
            return last_result.result_args[0]

    return TestController


@pytest.fixture
def test_users_controller_view(test_get_users_w_chg_names_controller, test_user_data_validator, test_users_dao):
    class UserswControllerView(BaseView):
        get_validator = test_user_data_validator
        # [NGibaev 15.04.19] overrides default ``collect`` behaviour
        get_controller = test_get_users_w_chg_names_controller
        post_validator_body = test_user_data_validator
        put_validator_body = test_user_data_validator
        delete_validator = test_user_data_validator
        model = test_users_dao

    return UserswControllerView
