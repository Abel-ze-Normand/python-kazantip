# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


# integration tests for concrete BaseView implementations

import pytest
import ujson
from typing import Type

from main import build_application
from common.utils.utils import change_dict_naming_convention, camel_to_underscore, dict_dig


@pytest.fixture
def test_db(request, loop, app):
    def cleanup():
        loop.run_until_complete(
            app.pool.execute("TRUNCATE TABLE __users;")
        )

    cleanup()  # fresh start
    loop.run_until_complete(
        app.pool.execute("""
        INSERT INTO __users VALUES
        (1, 'test_name', 'test_username', 'passw0rd'),
        (2, 'test_name_', 'test_username_', 'passw0rd');
        CLUSTER __users USING __users_pkey;
        """)
        # [NGibaev 11.04.19] cluster index to preserve order
    )
    request.addfinalizer(cleanup)


@pytest.fixture
def users_app_client(loop, aiohttp_client, test_users_view: Type['TestUsersView']):
    app = loop.run_until_complete(build_application())
    app.router.add_route('*', '/__test', test_users_view)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture
def users_app_controller_client(loop, aiohttp_client, test_users_controller_view):
    app = loop.run_until_complete(build_application())
    app.router.add_route('*', '/__test2', test_users_controller_view)
    return loop.run_until_complete(aiohttp_client(app))


async def test_get_model_all(test_db, loop, users_app_client):
    # [NGibaev 12.04.19] get all
    resp = await users_app_client.get('/__test')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert users
    assert list(map(lambda x: x['id'], users)) == [1, 2]
    assert dict_dig(data, ['data', 'total_items']) == len(dict_dig(data, ['data', 'items']))


async def test_get_model_ids(test_db, loop, users_app_client):
    # [NGibaev 12.04.19] get by ids
    resp = await users_app_client.get('/__test?ids=1')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert users
    assert list(map(lambda x: x['id'], users)) == [1, ]


async def test_get_model_fields(test_db, loop, users_app_client):
    # [NGibaev 12.04.19] get fields
    resp = await users_app_client.get('/__test?fields=id,name')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert users
    for u in users:
        assert set(u.keys()) == {'id', 'name'}


async def test_save_model(test_db, loop, users_app_client):
    in_data = dict(id=3, name='abc', username='abc', password='abcabc')
    resp = await users_app_client.post('/__test', json=in_data)
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert len(users) == 1
    assert tuple(in_data.values()) == tuple(users[0].values())

    resp = await users_app_client.get('/__test')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    assert len(dict_dig(data, ['data', 'items'])) == 3


async def test_put_model(test_db, loop, users_app_client):
    in_data = dict(id=2, name='abc', username='abc', password='abcabc')
    resp = await users_app_client.put('/__test', json=in_data)
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert len(users) == 1
    assert tuple(in_data.values()) == tuple(users[0].values())

    resp = await users_app_client.get('/__test?ids=2')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    # [NGibaev 12.04.19] drop last value -- DAO does not return passwords
    assert list(dict_dig(data, ['data', 'items', 0]).values()) == list(in_data.values())[:-1]


async def test_delete_model(test_db, loop, users_app_client):
    resp = await users_app_client.delete('/__test?ids=2')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    users = dict_dig(data, ['data', 'items'])
    assert len(users) == 1
    assert tuple(users[0].values()) == (2, 'test_name_', 'test_username_', 'passw0rd')

    resp = await users_app_client.get('/__test')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    assert len(dict_dig(data, ['data', 'items'])) == 1


async def test_view_with_controller(test_db, loop, new_name, users_app_controller_client):
    resp = await users_app_controller_client.get('/__test2')
    data = change_dict_naming_convention(ujson.loads(await resp.text()), camel_to_underscore)
    print(data)
    users = dict_dig(data, ['data', 'items'])

    # [NGibaev 15.04.19] if controller applied, then default behaviour with model is overriden
    for u in users:
        assert u['name'] == new_name
