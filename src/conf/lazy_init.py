# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import types

import conf.lazy_settings as ls


async def init_lazy_settings(app):
    init_funcs = [getattr(ls, a) for a in dir(ls)
                  if isinstance(getattr(ls, a), types.FunctionType) and a.startswith('set')]

    for func in init_funcs:
        await func(app)
