# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging

logger = logging.getLogger(__name__)

# EXAMPLE_CHOICES: list = []
#
#
# async def set_examples(app):
#     query = f"""
#     SELECT DISTINCT(name)
#     FROM exmples
#     """
#     records = await app.pool.fetch(query)
#     if records:
#         globals()['EXAMPLE_CHOICES'] = [record.upper() for (record, ) in records]
#     else:
#         logger.exception("ERROR INIT EXAMPLES")
#         # raise Exception("ERROR INIT EXAMPLES")
