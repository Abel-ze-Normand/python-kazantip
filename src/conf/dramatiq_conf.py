# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging

import dramatiq
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.brokers.stub import StubBroker

import conf.settings as settings
import common.utils.logger

__all__ = ['broker', 'ErrorMiddleware']


logger = logging.getLogger('main')


class ErrorMiddleware(dramatiq.Middleware):
    def after_process_message(self, broker, message, *, result=None, exception=None):
        if exception is not None:
            logger.exception(str(exception))

try:
    if settings.BROKER_URL and not settings.IS_TEST_MODE:
        logger.info("BROKER_URL set up, configuring AMQP broker...")
        broker = RabbitmqBroker(url=settings.BROKER_URL)
    else:
        logger.info("no BROKER_URL detected, setting up stub broker")
        broker = StubBroker()
        broker.emit_after("process_boot")
    broker.add_middleware(ErrorMiddleware())
    dramatiq.set_broker(broker)
except Exception:
    logger.exception('Setting dramatiq broker failed')
else:
    logger.info('Dramatiq broker configured')
