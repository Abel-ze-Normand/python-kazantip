# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging
import pkgutil
from importlib import import_module

import apps

logger = logging.getLogger('main')


def setup_routes(app):
    for importer, modname, ispkg in pkgutil.iter_modules(apps.__path__):
        try:
            # ищем во всех приложениях модуль routes и если есть регистрируем
            routes = import_module(f'apps.{modname}.routes')
            register(app, routes.routes)
            logger.debug(f'Routes for {modname} has been registered.')
        except Exception as e:
            logger.warning(f'Routes for {modname} has not been registered.',
                           exc_info=True)


def register(app, routes: list):
    for method, path, handler, name in routes:
        app.router.add_route(method, path, handler, name=name)
