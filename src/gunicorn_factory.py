# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import asyncio

import uvloop

from main import build_application


async def cons_app():
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    return await build_application()
