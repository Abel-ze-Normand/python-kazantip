#! /bin/sh

gunicorn gunicorn_factory:cons_app --bind 0.0.0.0:8080 --worker-class aiohttp.GunicornUVLoopWebWorker -w 4
