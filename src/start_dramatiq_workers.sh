#!/usr/bin/env bash
set -e
tasks_packages=$(find . -type d -name tasks | sed s':/:.:g' | sed s'/^..//' | xargs)
tasks_modules=$(find . -type f -name tasks.py | sed s':/:.:g' | sed s'/^..//' | sed s'/.py$//g' | xargs)
all_modules="$tasks_packages $tasks_modules"
echo "Discovered tasks modules:"
for module in $all_modules; do
    echo "  * ${module}"
done
echo

OPTIND=1         # Reset in case getopts has been used previously in the shell.

unset -v sub

while getopts ":Q:" opt; do
    case "$opt" in
    Q)  sub=("$OPTARG")
        until [[ $(eval "echo \${$OPTIND}") =~ ^-.* ]] || [ -z $(eval "echo \${$OPTIND}") ]; do
                sub+=($(eval "echo \${$OPTIND}"))
                OPTIND=$((OPTIND + 1))
        done
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [ -z "$sub" ]; then
    queues="default"
else
    queues=${sub[@]}
fi

echo "Queues: ${queues}"

dramatiq conf.dramatiq_conf $all_modules --watch-use-polling -Q $queues -p ${DRAMATIQ_PROCESSES:-2} -t ${DRAMATIQ_THREADS:-2}
